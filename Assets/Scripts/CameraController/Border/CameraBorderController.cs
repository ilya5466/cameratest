﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace CameraController.Border
{
    public class CameraBorderController : MonoBehaviour
    {
        [SerializeField] private Camera attachedCamera;
        [SerializeField] private CameraBorderParamters defaulCameraBorderParamters;

        public float MinOrtographicSize { get; private set; }
        public float MaxOrtographicSize { get; private set; }

        private List<BorderLine> cameraBorderLines;

        private Plane plane;
        private readonly Vector3 planeNormal = Vector3.up;

        private void Start()
        {
            attachedCamera = GetComponent<Camera>();
            CreateBorder(defaulCameraBorderParamters);
        }

        private void Update()
        {
            //CheckBorders();
        }

        public void CreateBorder(CameraBorderParamters borderParamters)
        {
            plane = new Plane(planeNormal, 0f);
            cameraBorderLines = new List<BorderLine>();
            for (int i = 0; i < borderParamters.BorderPoints.Count - 1; i++)
            {
                cameraBorderLines.Add(new BorderLine(ToVector3(borderParamters.BorderPoints[i]),
                    ToVector3(borderParamters.BorderPoints[i + 1])));
            }

            cameraBorderLines.Add(new BorderLine(
                ToVector3(borderParamters.BorderPoints[borderParamters.BorderPoints.Count - 1]),
                ToVector3(borderParamters.BorderPoints[0])));

            MinOrtographicSize = borderParamters.MinOrtographicSize;
            MaxOrtographicSize = borderParamters.MaxOrtographicSize;
        }


        public bool CheckBorders()
        {
            Vector3 positionBefore = transform.position;

            CheckBorder(0, 0);
            CheckBorder(0, 1);
            CheckBorder(1, 1);
            CheckBorder(1, 0);

            return transform.position != positionBefore;
        }


        private void CheckBorder(float x, float y)
        {
            Ray viewportRay = attachedCamera.ViewportPointToRay(new Vector3(x, y, 0));
            plane.Raycast(viewportRay, out float distanceToPlane);
            Vector3 hitPoint = viewportRay.origin +  viewportRay.direction * distanceToPlane;
            Debug.DrawLine(viewportRay.origin, hitPoint, Color.blue);

            Vector3 vectorToTranslate = Vector3.zero;
            foreach (BorderLine cameraBorderLine in cameraBorderLines)
            {
                (bool isOutside, Vector3 vectorToBorder) = GetBorderCheckResult(cameraBorderLine, hitPoint);
                if (isOutside)
                {
                    Debug.DrawLine(hitPoint, hitPoint + vectorToBorder, isOutside ? Color.yellow : Color.black);
                    vectorToTranslate += vectorToBorder;
                }
            }
            transform.transform.position += vectorToTranslate;
        }


        private (bool, Vector3) GetBorderCheckResult(BorderLine line, Vector3 pointToCheck)
        {
            Vector3 lineVector = line.End - line.Origin;
            Vector3 pointVector = pointToCheck - line.Origin;

            bool isOutside = Vector3.SignedAngle(lineVector, pointVector, plane.normal) < 0;

            Vector3 normalVector = pointVector - Vector3.Project(pointVector, lineVector);

            return (isOutside, -normalVector);
        }


        private Vector3 ToVector3(Vector2 vector2)
        {
            return new Vector3(vector2.x, 0, vector2.y);
        }

#if UNITY_EDITOR
        private void OnDrawGizmos()
        {
            if (cameraBorderLines != null)
            {
                Gizmos.color = Color.red;
                foreach (BorderLine cameraBorderLine in cameraBorderLines)
                {
                    Gizmos.DrawLine(cameraBorderLine.Origin, cameraBorderLine.End);
                }
            }
        }
#endif
    }

    public class BorderLine
    {
        public Vector3 Origin;
        public Vector3 End;

        public BorderLine(Vector3 origin, Vector3 end)
        {
            Origin = origin;
            End = end;
        }
    }
}
