﻿using System.Collections.Generic;
using UnityEngine;

namespace CameraController.Border
{
    [System.Serializable]
    public class CameraBorderParamters
    {
        public List<Vector2> BorderPoints;

        public float MinOrtographicSize;
        public float MaxOrtographicSize;
    }
}
