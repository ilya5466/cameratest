﻿using UnityEngine;

namespace CameraController.InputActions
{
    public interface IInputActions
    {
        bool Active { get;  }
        bool Pressed { get;  }
        bool Holds { get; }
        bool Released { get; }
        Vector2 Position { get; }
        Vector2 DeltaPosition { get;}
    }
}
