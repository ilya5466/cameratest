﻿using UnityEngine;

namespace CameraController.InputActions
{
    public class CombinedAnyInput : IInputActions
    {
        private IInputActions firstInput;

        private IInputActions mouseInput;

        public bool Active => firstInput.Active || mouseInput.Active;
        public bool Pressed => firstInput.Pressed || mouseInput.Pressed ;
        public bool Holds => firstInput.Holds || mouseInput.Holds;
        public bool Released => firstInput.Released || mouseInput.Released;

        public Vector2 Position
        {
            get
            {
                if (firstInput.Active)
                {
                    return firstInput.Position;
                }
                if (mouseInput.Active)
                {
                    return mouseInput.Position;
                }
                return  Vector2.zero;
            }
        }

        public Vector2 DeltaPosition
        {
            get
            {
                if (firstInput.Active)
                {
                    return firstInput.DeltaPosition;
                }

                if (mouseInput.Active)
                {
                    return mouseInput.DeltaPosition;
                }
                return Vector2.zero;
            }
        }

        public CombinedAnyInput(IInputActions firstInput, IInputActions mouseInput)
        {
            this.firstInput = firstInput;
            this.mouseInput = mouseInput;
        }

   
    }
}
