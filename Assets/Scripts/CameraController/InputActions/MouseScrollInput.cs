﻿using UnityEngine;

namespace CameraController.InputActions
{
    public class MouseScrollInput : IInputActions
    {
        public bool Active => Input.mouseScrollDelta != Vector2.zero;
        public bool Pressed => Input.mouseScrollDelta != Vector2.zero;
        public bool Holds => Input.mouseScrollDelta != Vector2.zero;
        public bool Released => Input.mouseScrollDelta == Vector2.zero;

        public Vector2 Position => Input.mousePosition;
        public Vector2 DeltaPosition => Input.mouseScrollDelta;
    }
}


