﻿using UnityEngine;

namespace CameraController.InputActions
{
    public class CombinedBothInput : IInputActions
    {
        private IInputActions firstInput;
        private IInputActions secondInput;

   
        public bool Active => firstInput.Active && secondInput.Active;
        public bool Pressed => firstInput.Pressed && secondInput.Pressed;
        public bool Holds => firstInput.Holds && secondInput.Holds;
        public bool Released => firstInput.Released && secondInput.Released;

        public Vector2 Position => ((firstInput.Position + secondInput.Position) / 2f);

        public Vector2 DeltaPosition => (firstInput.DeltaPosition + secondInput.DeltaPosition) / 2f;

        public CombinedBothInput(IInputActions firstInput, IInputActions secondInput)
        {
            this.firstInput = firstInput;
            this.secondInput = secondInput;
        }
    }
}
