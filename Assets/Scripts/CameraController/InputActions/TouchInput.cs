﻿using UnityEngine;

namespace CameraController.InputActions
{
    public class TouchInput : IInputActions
    {
        // Memory of number touch;
        private int touchNumber;

        public bool Active => Input.touchCount > touchNumber;
        public bool Pressed => Input.touches[touchNumber].phase == TouchPhase.Began;
        public bool Holds => Input.touches[touchNumber].phase == TouchPhase.Stationary;
        public bool Released => Input.touches[touchNumber].phase == TouchPhase.Ended;

        public  Vector2 Position
        {
            get => Input.GetTouch(touchNumber).position;
        }
        public  Vector2 DeltaPosition
        {
            get => Input.GetTouch(touchNumber).deltaPosition;
        }

        //Constructors
        public TouchInput(int touchNumber) => this.touchNumber = touchNumber;
        public TouchInput() => this.touchNumber = 0;
    }
}
