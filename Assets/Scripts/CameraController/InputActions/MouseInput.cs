﻿using UnityEngine;

namespace CameraController.InputActions
{
    public class MouseInput :  IInputActions
    {
        private Vector2 currentMousePos;
        public Vector2 lastMousePos = Vector2.zero;
        private Vector2 deltaMousePos;
        private Plane plane;
        public  Camera camera;

        public bool Active => Input.GetMouseButton(0);
        public bool Pressed => Input.GetMouseButtonDown(0);
        public bool Holds => Input.GetMouseButton(0);
        public bool Released => Input.GetMouseButtonUp(0);

        public Vector2 Position
        {
            get { return Input.mousePosition; }
        }
        public Vector2 DeltaPosition
        {
            get
            {
                currentMousePos = ToVector2(Input.mousePosition);
                deltaMousePos = currentMousePos - lastMousePos;
                lastMousePos = currentMousePos;
            
                return deltaMousePos;
            }
        }
        public MouseInput(Camera cam)
        {
            camera = cam;
        }

        private Vector2 ToVector2(Vector3 pos)
        {
            return new Vector2(pos.x,pos.y);
        }
    }
}
