﻿using UnityEngine;

namespace CameraController.Core
{
    public class CameraInputConroller : MonoBehaviour
    {
        [SerializeField] private CameraMovementController cameraMovementController;
        [SerializeField] private float mouseScrollMultiplier = 10f;

        void Update()
        {
            ProcessInput();
        }

        private void ProcessInput()
        {
            ProcessMouseInput();
            ProcessTouchInput();
        }

        private void ProcessMouseInput()
        {
            if (GameInput.SingleInput.Active && !GameInput.DoubleInput.Active)
            {
                cameraMovementController.ProcessMovement(GameInput.SingleInput.DeltaPosition);
            }

            if (!GameInput.SingleInput.Active)
            {
                cameraMovementController.ProcessIntertia();
            }

            if (GameInput.MouseScrollInput.Active)
            {
                cameraMovementController.ProcessZoom(GameInput.MouseScrollInput.Position,
                    GameInput.MouseScrollInput.DeltaPosition * mouseScrollMultiplier, GameInput.MouseScrollInput.DeltaPosition.y > 0);
            }
        }

        private void ProcessTouchInput()
        {
            if (GameInput.FirstInputTouch.Active)
            {
                Vector2 firstInputPosition = GameInput.FirstInputTouch.Position;
                Vector2 firstInputDelta = GameInput.FirstInputTouch.DeltaPosition;

                Vector2 secondInputPosition = firstInputPosition;
                Vector2 secondInputDelta = firstInputDelta;

                bool isPositive = false;

                if (GameInput.SecondInputTouch.Active)
                {
                    secondInputPosition = GameInput.SecondInputTouch.Position;
                    secondInputDelta = GameInput.SecondInputTouch.DeltaPosition;

                    isPositive =
                        Vector3.Distance(firstInputPosition + firstInputDelta, secondInputPosition + secondInputDelta) >
                        Vector3.Distance(firstInputPosition, secondInputPosition);
                    
                    cameraMovementController.ProcessMovement((firstInputDelta + secondInputDelta) / 2);
                    cameraMovementController.ProcessZoom((firstInputPosition + secondInputPosition) / 2,
                    (firstInputDelta - secondInputDelta) / 2, isPositive);
                }



                
            }
        }
    }
}