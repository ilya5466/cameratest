﻿using CameraController.Border;
using UnityEngine;
using UnityEngine.Serialization;

namespace CameraController.Core
{
    public class CameraMovementController : MonoBehaviour
    {
        [Header("Attachments")]
        [SerializeField] private CameraBorderController cameraBorder;
        [SerializeField] private Camera attachedCamera;
        
        [Header("Set in Inspector: touch speed")]
        [SerializeField] [FormerlySerializedAs("speedScreenTouch")] private float zoomSpeed;
        [SerializeField] [FormerlySerializedAs("speedMoveTouch")] private float movementSpeed;
        
        [Header("Inertia")]
        [SerializeField] [FormerlySerializedAs("speedInertia")] private float intertiaSpeed;
        [SerializeField] [FormerlySerializedAs("lengthInertia")] private float inertiaLength;

        private float pixelSize;
        private Vector2[] previousMovements = new Vector2[10];
        private int countOfPreviousMovements = 0;
        private Vector3 targetIntertiaPosition;

        private void Awake()
        {
            targetIntertiaPosition = transform.position;
            
            pixelSize = attachedCamera.pixelRect.size.magnitude;
            ClearPreviousMovements();
        }

        private void Start()
        {
            //cam.orthographicSize = cameraBorder.maxOrtographicSize;
        }

        public void ProcessMovement(Vector2 delta)
        {
            // Текущее движение
            float actualMovementSpeed = 2 * movementSpeed * attachedCamera.orthographicSize / pixelSize;
            float deltaPosX = -delta.x * actualMovementSpeed;
            float deltaPosY = -delta.y * actualMovementSpeed;
            Vector3 movementVector = new Vector3(deltaPosX + deltaPosY, 0, -deltaPosX + deltaPosY);
            transform.position += movementVector;

            AddMovementInstance(-delta);
            cameraBorder.CheckBorders();

            // Вычисление инерции
            Vector2 averageArrayOfTouches = CalculateAverageOfMovements();
            float actualIntertiaSpeed = 2 * inertiaLength * attachedCamera.orthographicSize / pixelSize;
            float deltaIntertiaX = averageArrayOfTouches.x * actualIntertiaSpeed;
            float deltaIntertiaY = averageArrayOfTouches.y * actualIntertiaSpeed;

            Vector3 intertiaVector = new Vector3(deltaIntertiaX + deltaIntertiaY, 0f, -deltaIntertiaX + deltaIntertiaY);
            targetIntertiaPosition = transform.position + intertiaVector;
        }

        public void ProcessIntertia()
        {
            if (transform.position != targetIntertiaPosition)
            {
                transform.position = Vector3.Lerp(transform.position, targetIntertiaPosition,
                    Time.deltaTime * intertiaSpeed);
                ClearPreviousMovements();
                bool isPositionChanged = cameraBorder.CheckBorders();
                if (isPositionChanged)
                {
                    targetIntertiaPosition = transform.position;
                }
            }
        }

        public void ProcessZoom(Vector2 positionZoomCenter, Vector2 difference, bool isPositive) =>
            ProcessZoom(positionZoomCenter, difference.magnitude * (isPositive ? 1 : -1));

        public void ProcessZoom(Vector2 positionZoomCenter, float difference) // For touches
        {
            ClearPreviousMovements();

            // Серединный вектор между пальцами - то место, которое доложно стать центром в камере
            Vector3 zoomCenter = attachedCamera.ScreenToWorldPoint(positionZoomCenter);
            zoomCenter.y = transform.position.y;
            //Движение камеры к центру 
            attachedCamera.orthographicSize =
                Mathf.Clamp(attachedCamera.orthographicSize - difference * zoomSpeed,
                    cameraBorder.MinOrtographicSize, cameraBorder.MaxOrtographicSize);
            transform.position = Vector3.Lerp(transform.position, zoomCenter,
                difference * zoomSpeed * Time.deltaTime);
            targetIntertiaPosition = transform.position;
            cameraBorder.CheckBorders();
        }


        //For Array of last deltaTouchPosition
        private void AddMovementInstance(Vector2 deltaTouch)
        {
            if (countOfPreviousMovements == previousMovements.Length)
            {
                countOfPreviousMovements = 0;
            }
            previousMovements[countOfPreviousMovements] = deltaTouch;
            countOfPreviousMovements += 1;
        }

        private void ClearPreviousMovements()
        {
            for (int i = 0; i < previousMovements.Length; i++)
            {
                previousMovements[i] = Vector2.zero;
            }
        }

        private Vector2 CalculateAverageOfMovements()
        {
            Vector2 average = Vector2.zero;
            foreach (var deltaPosition in previousMovements)
            {
                average += deltaPosition;
            }
            return average / previousMovements.Length;
        }
    }
}
    