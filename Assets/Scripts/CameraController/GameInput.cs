﻿using CameraController.InputActions;
using UnityEngine;

namespace CameraController
{
    public class GameInput : MonoBehaviour
    {
        public static IInputActions FirstInputTouch { get; private set; }
        public static IInputActions SecondInputTouch { get; private set; }


        private static MouseInput mouse;

        public static IInputActions SingleInput { get; private set; }
        public static IInputActions DoubleInput { get; private set; }
        public static IInputActions MouseScrollInput { get; private set; }


        private void Start()
        {
            FirstInputTouch = new TouchInput();
            SecondInputTouch = new TouchInput(1);
            mouse = new MouseInput(GetComponent<Camera>());
            MouseScrollInput = new MouseScrollInput();

            SingleInput = new CombinedAnyInput(FirstInputTouch, mouse);
            DoubleInput = new CombinedBothInput(FirstInputTouch, SecondInputTouch);
        }

        private void Update()
        {
            if (!mouse.Active)
            {
                mouse.lastMousePos = mouse.Position;
            }
        }
    }
}
